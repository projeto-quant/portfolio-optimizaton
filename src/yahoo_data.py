import pandas as pd
import yfinance as yf
from src.utils.get_conf import get_api_key

def fetch_data(assets):
    
    '''
    Parameters:
    -----------
        assets: list 
            takes in a range of ticker symbols to be pulled from Yahoo! finance.  
            Note: It's imporant to make the data retrieval flexible to multiple assets because
            we will likely want to back-test strategies based on cross-asset technical 
            indictors are ratios!  It's always a good idea to put the work and thought 
            in upfront so that your functions are as useful as possible.
            
    Returns:
    --------
        df: Dataframe 
            merged market data from Quandl using the date as the primary merge key.
    '''
        
    count = 0
    
    #instatiate an empty dataframe
    df = pd.DataFrame()
    
    # Because we will potentially be merging multiple tickers, we want to rename
    # all of the income column names so that they can be identified by their ticker name.  
    for asset in assets:  
        if (count == 0):
            print(asset) 
            df = yf.Ticker(asset).history(period='2y').reset_index()[['Date', 'Close']]
            column_names=list(df)

            for i in range(len(column_names)):
                if column_names[i] != 'Date':
                    column_names[i] = asset + '_' + column_names[i]
            df.columns = column_names
            print(df.columns)   
            count += 1      
        else:
            print(asset)         
            temp = yf.Ticker(asset).history(period='2y').reset_index()[['Date', 'Close']] 
            column_names=list(temp)

            for i in range(len(column_names)):
                if column_names[i] != 'Date':
                    column_names[i] = asset + '_' + column_names[i]
            
            temp.columns = column_names
            print(temp.columns)  
            # Merge all the dataframes into one with new column names
            df = pd.merge(df, temp, how = 'left', on='Date')
        
    return df