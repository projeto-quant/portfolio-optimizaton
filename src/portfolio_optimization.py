from src.portfolio_stats import portfolio_stats

def minimize_sharpe(simulated_weights, *returns):  
    return -portfolio_stats(simulated_weights, *returns)['sharpe']

def minimize_volatility(simulated_weights, *returns):  
    # Note that we don't return the negative of volatility here because we 
    # want the absolute value of volatility to shrink, unlike sharpe.
    return portfolio_stats(simulated_weights, *returns)['volatility']