import time
import numpy as np
import matplotlib.pyplot as plt

def portfolio_simulation(asset_returns, iterations):
    '''
    Runs a simulation by randomly selecting portfolio weights a specified
    number of times (iterations), returns the list of results and plots 
    all the portfolios as well.
    
    Parameters:
    -----------  
        assets_returns: list
            all the assets that are to be pulled from Yahoo! Finance to comprise
            our portfolio.    
        iterations: int 
            the number of randomly generated portfolios to build.
    
    Returns:
    --------
        port_returns: array
            array of all the simulated portfolio returns.
        port_vols: array
            array of all the simulated portfolio volatilities.
        port_weights: array
            array of all the simulated portfolio weights.

    '''
    
    start = time.time()
    num_assets = len(asset_returns.columns)
    
    port_returns = []
    port_vols = []
    port_weights = []
    
    for i in range (iterations):
        weights = np.random.dirichlet(np.ones(num_assets),size=1)
        weights = weights[0]
        port_weights.append([weights])
        port_returns.append(np.sum(asset_returns.mean() * weights) * 252)
        port_vols.append(np.sqrt(np.dot(weights.T, np.dot(asset_returns.cov() * 252, weights))))
    
    # Convert lists to arrays
    port_returns = np.array(port_returns)
    port_vols = np.array(port_vols)

    # Optimum values
    optimum_weights = port_weights[(port_returns/port_vols).argmax()][0]
    optimum_port_return = np.sum(asset_returns.mean()*optimum_weights)*252
    port_var = np.dot(optimum_weights.T, np.dot(asset_returns.cov()*252, optimum_weights))
    port_vol = np.sqrt(port_var) 
    
    # Plot the distribution of portfolio returns and volatilities 
    plt.figure(figsize=(10,6))

    plt.scatter(port_vols,
                port_returns,
                c = (port_returns / port_vols),
                marker = 'o')

    plt.plot(port_vol,
            optimum_port_return,
            'r*',
            markersize = 25.0)

    plt.xlabel('Portfolio Volatility')
    plt.ylabel('Portfolio Return')
    plt.colorbar(label='Sharpe ratio')
    
    print('Elapsed Time: %.2f seconds' % (time.time() - start))
    
    return port_returns, port_vols, port_weights