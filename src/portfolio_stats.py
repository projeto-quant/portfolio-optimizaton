import numpy as np
import pandas as pd

def portfolio_stats(simulated_weights, *returns):
    
    '''
    We can gather the portfolio performance metrics for a specific set of weights.
    This function will be important because we'll want to pass it to an optmization
    function to get the portfolio with the best desired characteristics.
    
    Note: Sharpe ratio here uses a risk-free short rate of 0.
    
    Paramaters: 
    -----------
        simulated_weights: array, 
            asset weights in the portfolio.
        returns: dataframe
            an array of returns for each asset in the trial portfolio    
    
    Returns: 
    --------
        dict of portfolio statistics - mean return, volatility, sharp ratio.
    '''

    returns = returns[0]

    # Convert to array in case list was passed instead.
    weights = np.array(simulated_weights) 
    port_return = np.sum(returns.mean(axis=1)* weights) * 252
    port_vol = np.sqrt(np.dot(weights.T, np.dot(np.cov(returns) * 252, weights)))
    sharpe = port_return/port_vol

    return {'return': port_return, 'volatility': port_vol, 'sharpe': sharpe}