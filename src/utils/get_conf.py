'''Centralize credentials and encrypt key reading'''
import os
import json


def get_api_key() -> str:
    ''' Load configurations from global resources,
        for local testing
    '''

    gs_filename = "conf/globalresources.json"

    with open(gs_filename, encoding='utf-8-sig') as json_file:
        key = json.load(json_file)['api-key']

    return key